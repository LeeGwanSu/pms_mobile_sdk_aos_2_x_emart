package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;

public class SetMsgKind extends BaseRequest {

	public SetMsgKind(Context context) {
		super(context);
	}

	public JSONObject getParam (String flag, String kind) {

		JSONObject json = new JSONObject();
		try {
			json.put("Flag", flag);
			json.put("Kind", kind);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (String flag, String kind, final APICallback apiCallback) {
		try {
			apiManager.call(API_SET_MSGKIND_PMS, getParam(flag, kind), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}
